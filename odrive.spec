%global _build_id_links none
%define debug_package %{nil}

Name:		odrive
Version:	0.2.2
Release:	1%{?dist}
Summary:	OpenSource Drive - ODrive - Google Drive GUI for Linux

License:	GPL-3.0
URL:		https://github.com/liberodark/ODrive
Source0:    %{url}/archive/%{version}.zip

BuildRequires: desktop-file-utils
BuildRequires: libappstream-glib
BuildRequires: nodejs npm nodejs-yarn python gcc-c++ snapd
ExclusiveArch: x86_64 i686 i586 i386

%description
ODrive is a GUI client for Google Drive on linux application based on the https://electron.atom.io/.

%prep
%setup -n ODrive-%{version}

%build
yarnpkg install
yarnpkg build

%install
# Create directories
install -d %{buildroot}%{_libdir}/%{name}
install -d -m755 -p %{buildroot}%{_bindir}
install -d %{buildroot}%{_datadir}/%{name}
install -d %{buildroot}%{_datadir}/applications
install -d %{buildroot}%{_sysconfdir}/ld.so.conf.d

%ifarch x86_64 amd64
  %define linuxunpacked dist/linux-unpacked
%else
  %define linuxunpacked dist/linux-ia32-unpacked
%endif

cp -a %{linuxunpacked}/* %{buildroot}%{_datadir}/%{name}

# a little ugly - symbolic link creation
ln -s %{_datadir}/%{name}/%{name} %{buildroot}%{_bindir}/%{name}

install -D -m644 -p aur/%{name}-bin.png %{buildroot}%{_datadir}/pixmaps/%{name}.png

sed -i 's!Exec=.*!Exec=/usr/bin/odrive %U!' aur/odrive-bin.desktop
sed -i 's!Icon=.*!Icon=odrive!' aur/odrive-bin.desktop
install -D -m644 -p aur/odrive-bin.desktop %{buildroot}%{_datadir}/applications/odrive.desktop
desktop-file-validate %{buildroot}%{_datadir}/applications/odrive.desktop
#install -D -m644 -p %{name}.appdata.xml %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml
#appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/*.appdata.xml

install -D -m755 -p %{buildroot}%{_datadir}/%{name}/*.so %{buildroot}%{_libdir}/%{name}/
rm %{buildroot}%{_datadir}/%{name}/*.so

echo "%{_libdir}/%{name}" > etc-ld.so.conf.d_%{name}.conf
install -D -m644 -p etc-ld.so.conf.d_%{name}.conf %{buildroot}%{_sysconfdir}/ld.so.conf.d/%{name}.conf

%post
umask 007
/sbin/ldconfig > /dev/null 2>&1
/usr/bin/update-desktop-database &> /dev/null || :

%postun
umask 007
/sbin/ldconfig > /dev/null 2>&1
/usr/bin/update-desktop-database &> /dev/null || :

%files
%defattr(-,root,root,-)
%license LICENSE.md
%{_datadir}/%{name}
%{_datadir}/pixmaps/%{name}.png
%{_datadir}/applications/%{name}.desktop
#%{_datadir}/metainfo/%{name}.appdata.xml
%{_bindir}/%{name}
%dir %attr(755,root,root) %{_libdir}/%{name}
%{_libdir}/%{name}/*.so
%{_sysconfdir}/ld.so.conf.d/%{name}.conf

%changelog
